# VSCoder Experimentals
>This project contains experimental bundles and other features related to
[__VSCoder__](https://gitlab.com/daemonraco-dev/vscoder).
On itself it doesn't provide much value.

To enable _experimentals_ in your _VSCoder_ container, run
`dev-helper enable-experimentals` and it will clone all experimental bundles,
features and configurations, and it'll will make it available for you to use with `dev-helper`.

## Bundles
This provides these installable bundles:

* `dev-helper install-java`: It installs Oracle Java v17 and configures
environment variables.

## License
This image is distributed under the MIT license and the author is Alejandro Dario
Simi. You can read the license disclaimer [here](LICENSE).