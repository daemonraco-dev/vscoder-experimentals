#!/bin/bash
. /usr/share/dev-helper/dev-helper.constants.sh;
#
curDir="$PWD";
javaVersion="17";
#
if [ ! -e "/usr/lib/jvm/jdk-${javaVersion}" ] && [ -z "$(which java)" ]; then
    #
    # Working directory.
    if [ -e /tmp/java-installation ]; then
        sudo rm -fr /tmp/java-installation;
    fi;
    mkdir /tmp/java-installation;
    cd /tmp/java-installation;
    #
    # Downloading required packages.
    sudo apt-get install libc6-i386 libc6-x32 libfreetype6 libxi6 libxrender1 libxtst6 libpng16-16 x11-common;
    #
    # Downloading JDK from Oracle.
    wget "https://download.oracle.com/java/${javaVersion}/latest/jdk-${javaVersion}_linux-x64_bin.deb";
    #
    # Installing JDK.
    sudo dpkg --install "jdk-${javaVersion}_linux-x64_bin.deb";
    #
    # Setting JAVA_HOME and PATH.
    echo "#"                                                   | sudo tee -a /dev-helper/memory/bashrc >/dev/null;
    echo "# Java Environment variables:"                       | sudo tee -a /dev-helper/memory/bashrc >/dev/null;
    echo "export JAVA_HOME='/usr/lib/jvm/jdk-${javaVersion}';" | sudo tee -a /dev-helper/memory/bashrc >/dev/null;
    echo "export PATH=\"\$PATH:\$JAVA_HOME/bin\";"             | sudo tee -a /dev-helper/memory/bashrc >/dev/null;
    #
    # Remembering.
    /bin/bash /usr/share/dev-helper/dev-helper.memory.sh install java;
    #
    cd "$curDir";
    #
    echo -e "\n\e[33mSome system configurations have changed.\e[0m" >&2;
    echo -e "\e[33mIt is recommended that you close your current session and log back in.\e[0m" >&2;
else
    echo -e "\n\e[33mJava is already installed.\e[0m" >&2;
fi;
#
# Installing extensions.
/bin/bash /usr/share/dev-helper/install-default-extensions.sh;
/bin/bash $DEV_HELPER_EXPERIMENTALS_DIR/scripts/install-java-extensions.sh;
